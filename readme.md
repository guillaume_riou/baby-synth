# Baby Synth

A synth for babies (2-3 years probably)

## TODO

### meta
- Find a better name

### Features

- 3x2 "modulation matrix" wiht magnetic connectors
- One big encoder in the middle
- 12 switches with aftertouch with a note mappig that minimizes dissonnance between clustered keys
- lipo battery and charger
- enclosure mounted piezo for impact based modulation
- integrated mono speaker

### todolist

#### mod matrix
- [x] Buy connectors
- [ ] figure out resistor values
- [ ] test on proto board
- [ ] see if it needs a real pcb (maybe not)
- [ ] brainstorm the possible modulations

#### big encoder
- [x] figure out which encoder
- [x] buy it
- [ ] buy a logic level conversion breakout board for it :(((
- [ ] buy a step up converer for it
- [ ] make a big knob for it
- [ ] figure out a parameter for it (lowpass cutoff or something really splashy)

#### keys
- [x] Plan key layout
- [x] modelize keycap
- [x] 3d print keycap poc
- [ ] update openscad file with diameter modifications
- [ ] figure out matrix circuit
- [ ] test matrix circuit on proto board/arduino
- [ ] make a pcb with kicad
- [ ] figure out mounting points of pcb and add fsr at the right spot
- [ ] food safe epoxy the caps
- [ ] write working code to work with it
- [ ] figure out a good note mapping

#### lipo battery
- [ ] buy a pretty big battery
- [ ] figure out how to make it work with the feather m4
- [ ] usb connection : rare earth magnetic cap on on enclosure with neoprene for the seal ????
- [ ] figure out an on/off switch

#### enclosure mounted piezo
- [x] buy a piezo
- [ ] [figure out the right wiring](https://forum.arduino.cc/t/how-to-wire-a-piezo-as-an-impact-sensor/901726/22)
- [ ] proto board it
- [ ] write code for it

#### integrated speaker
- [x] buy i2s 3 watt amplifier
- [x] buy 3 watt speaker
- [ ] figure out the audio samples -> i2s pipeline
- [ ] try it
- [ ] figure out a grill for it

#### enclosure
- [ ] figure out the dimensions
- [ ] figure out mounting points for everything
- [ ] water proof everything

#### Stuff to buy
- [x] food safe epoxy https://www.artresin.com/products/32oz-artresin-starter-kit
- [x] feather m4 mcu
- [x] proto boards
- [x] wire

####
