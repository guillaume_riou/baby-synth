// Thanks https://www.thingiverse.com/timbassler for the
// post code and thanks to
// https://www.thingiverse.com/rsheldiii/ for the
// measurements in key v2
use <bevel_extrude.scad>

// This makes an extruded hexagon. If the hexagon gets split in 
// two parts, increase the margin parameter slightly (0.001).
// Increasing margin will make the shapes measurements less precise ...
// "bevel_intensity" controls the angle of the bevels. I don't know what the units are, just mess around with it until it works for you. Values between 0 and 3 seem to produce interesting results.
module hex (x_center, y_center, z, hex_full_length, height, bevel_intensity=0.7, margin=0.0) {
    // one of the hexagon's triangles side length
    triangle_side = (hex_full_length / 2);
    margin = margin*triangle_side ;
    triangle_side_margined = triangle_side + margin;
    translate([x_center, y_center, z]) {
        bevel_extrude(height=height,bevel_depth=height, bevel_percent=bevel_intensity,$fn=16)
        union() {
            for(i = [0 : 60 : 360]) {
                rotate([0,0,i]) {
                    polygon([
                        [0, 0],
                        [0, -triangle_side_margined],
                        [cos(30)*(triangle_side_margined), -sin(30)*(triangle_side_margined)]]
                    );
                }
            }
        }
    }
}


// This makes a (hopefully) cherry mx compatible post 
module mx_post(x, y, z, height,$fn = 36) {
    slop = 0.3;
    postDiam = 6.4;
    crossHLong = 4.03 + slop;
    crossVLong = 4.23 + slop;
    crossHWide = 1.25 + slop / 3;
    crossVWide = 1.15 + slop / 3;
    difference() {
    
        translate([x, y, z])
            cylinder(h=height, d=postDiam);
        translate([x, y, z])
            // height*2.01 just to get rid of render artifact.
            cube([crossHWide, crossHLong, height*2.01], center=true);
        translate([x, y, z])
            cube([crossVLong, crossVWide, height*2.01], center=true);
    } // end difference

}
// length of the hexagon from corner to corner
hex_fullheight = 25.3;
cap_height = 6.5;
cap_depth = 5;
stemThrow = 4;
// cap
union () {
    difference() {
        hex(0, 0, 0, hex_fullheight, cap_height, margin=0.000001);
        hex(0, 0, 0, hex_fullheight*0.8, cap_depth);
    }
    // center post
    mx_post(0,0,cap_depth - stemThrow, stemThrow );
}
