// Thanks https://www.thingiverse.com/gregfrost for the code
// This is a slightly modified version with a parameter added for bevel angle.

bevel_extrude_example();

module bevel_extrude_example()
{
	bevel_extrude(height=10,bevel_depth=2,$fn=16)
	example_shape();
}

module example_shape()
{
	union()
	{
		square([8,30],true);
		rotate(30)
		square([35,8],true);

		translate([0,15])
		square([30,8],true);
	}
}

module bevel_border(size=1)
{
	difference()
	{
		minkowski()
		{
			children();
			square(size,true);
		}
		children();
	}
}

module bevel_cutaway(bevel_depth=5, bevel_percent)
{
	translate([0,0,-bevel_depth])
	minkowski()
	{
		linear_extrude(height=bevel_depth,convexity=5)
		bevel_border(1)
		children();
		cylinder(h=bevel_depth+1,r1=0,r2=bevel_depth*bevel_percent);
	}
}

module bevel_extrude(height=20,bevel_depth=1,bevel_percent=0.9)
{
	difference()
	{
		linear_extrude(height=height,convexity=5)
		children();

		translate([0,0,height])
                    bevel_cutaway(bevel_depth, bevel_percent)
		children();
	}
}
